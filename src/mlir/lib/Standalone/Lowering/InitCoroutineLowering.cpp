#include "Standalone/ConstantManager.h"
#include "Standalone/Lowering/MyBuilder.h"
#include "Standalone/AllIncludes.h"
#include "Standalone/Lowering/AssertOperator.h"
#include "Standalone/Lowering/Printer.h"

#include "Standalone/StandalonePasses.h"
#include "Standalone/StandalonePrerequisites.h"
#include "Standalone/TypeDefinitions.h"


namespace mlir::standalone::passes {
    LogicalResult InitCoroutineLowering::matchAndRewrite(InitCoroutine txnOp, PatternRewriter &rewriter) const {
        auto op = &txnOp;
        LOWERING_PASS_HEADER
        LOWERING_NAMESPACE

        ConstantManager constants(rewriter, ctx);
        MyBuilder builder(ctx, constants, rewriter);
        MyAssertOperator myAssert(rewriter, constants, ctx, __FILE_NAME__);
        Printer print(ctx, rewriter, __FILE_NAME__);
        myOperators

        auto pc = txnOp.pcAttr().getUInt();
        auto p1 = txnOp.p1Attr().getSInt();
        auto p3 = txnOp.p3Attr().getSInt();

        auto jumpTo = txnOp.jumpTo();
        auto fallThrough = txnOp.fallThrough();

        USE_DEFAULT_BOILERPLATE

        auto firstBlock = rewriter.getBlock();
        auto curBlock = rewriter.getBlock();
        auto endBlock = curBlock->splitBlock(txnOp); GO_BACK_TO(curBlock);

        /// pOut = &aMem[pOp->p1];
        auto pOut = getElementPtrImm(LOC, T::sqlite3_valuePtrTy, vdbeCtx->aMem, p1);

        /// pOut->u.i = pOp->p3 - 1;
        auto pOutUAddr = getElementPtrImm(LOC, T::doublePtrTy, pOut, 0, 0);
        auto pOutIntegerAddr = bitCast(LOC, pOutUAddr, T::i64PtrTy);
        store(LOC, p3 - 1, pOutIntegerAddr);

        /// pOut->flags = MEM_Int;
        auto flagsAddr = getElementPtrImm(LOC, T::i16PtrTy, pOut, 0, 1);
        store(LOC, MEM_Int, flagsAddr);

        branch(LOC, endBlock);

        ip_start(endBlock);
        branch(LOC, jumpTo);
        rewriter.eraseOp(txnOp);

        return success();
    } // matchAndRewrite
} // namespace mlir::standalone::passes