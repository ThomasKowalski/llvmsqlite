--- Query with ORDER BY
-- This allows to check if the Sorter module 
-- is more / less efficient in the JIT.
SELECT SI_I FROM SaleItem ORDER BY SI_S;
