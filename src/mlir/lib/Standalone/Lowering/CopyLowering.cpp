#include "Standalone/ConstantManager.h"
#include "Standalone/Lowering/MyBuilder.h"
#include "Standalone/Lowering/AssertOperator.h"
#include "Standalone/Lowering/Printer.h"

#include "Standalone/StandalonePasses.h"


namespace mlir::standalone::passes {
    LogicalResult CopyLowering::matchAndRewrite(Copy copyOp, PatternRewriter &rewriter) const {
        auto op = &copyOp;
        LOWERING_PASS_HEADER
        LOWERING_NAMESPACE

        ConstantManager constants(rewriter, ctx);
        MyBuilder builder(ctx, constants, rewriter);
        MyAssertOperator myAssert(rewriter, constants, ctx, __FILE_NAME__);
        Printer print(ctx, rewriter, __FILE_NAME__);
        myOperators

        print(LOCL, "-- Copy");

        auto firstBlock = rewriter.getBlock();

        auto firstFromRegAttr = copyOp.firstFromRegAttr();
        auto nFromRegAttr = copyOp.nFromRegAttr();
        auto firstToRegAttr = copyOp.firstToRegAttr();

        auto firstFromReg = firstFromRegAttr.getSInt();
        auto nReg = nFromRegAttr.getSInt() + 1;
        auto firstToReg = firstToRegAttr.getSInt();
        auto pc = copyOp.pcAttr().getUInt();

        USE_DEFAULT_BOILERPLATE

        auto curBlock = rewriter.getBlock();
        auto endBlock = curBlock->splitBlock(copyOp); GO_BACK_TO(curBlock);

        for(int i = 0; i < nReg; i++) {
            auto pIn = getElementPtrImm(LOC, T::sqlite3_valuePtrTy, vdbeCtx->aMem, firstFromReg + i);
            auto pOut = getElementPtrImm(LOC, T::sqlite3_valuePtrTy, vdbeCtx->aMem, firstToReg + i);

            call(LOC, f_sqlite3VdbeMemShallowCopy, pOut, pIn, constants(MEM_Ephem, 32));
        }

        branch(LOC, endBlock);

        ip_start(endBlock);
        rewriter.eraseOp(copyOp);

        return success();
    } // matchAndRewrite
} // namespace mlir::standalone::passes