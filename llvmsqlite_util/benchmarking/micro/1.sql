--- Simple select
-- This allows to test whether the performance of 
-- going through the entire table and generating many 
-- tuples differs greatly. 
SELECT * FROM Items;
